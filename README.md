# OpenCPSI-S.x Pinouts

Open Common Parallel Serial Interconnect - Solution for modular electronic systems

OpenCPSI-S is a recommendation based on the PICMG® CompactPCI® Serial Specification [1].

CompactPCI Serial (CPCI-S.0) is a specification for modular computer systems
with a large number of rear user I/O pins for peripheral boards. It offers flexible
topologies from single CPU centric systems to multiprocessing clusters and suits
a wide range of applications. Leaving the I/O area definition of the
connectors (P2-P5) to the user, introduces however an interoperability problem
between CPCI-S.0 peripheral and rear boards.


OpenCPSI-S aims to improve the interoperability of peripheral and rear boards
and plans to enhance the field of applications to highspeed modular electronic
systems with continuous time communication capabilities between peripheral
boards beyond the packetized communication via PCIe or Ethernet. Additionally
OpenCPSI-S provides a optional and simple system management and monitoring concept.
OpenCPSI-S.x pinouts are recommendations for similar classes of boards. They hall be compliant to the CPCI-S.0 R2.0 peripheral board pin assignment.

## General common definition of pins for:
- Power transmission to rear boards with compatibility to system slot
- Single wire EEPROM interface for identification of rear board
- Power enable signal to rear board

## OpenCPSI-S.1 : System Monitor Pinout
- System monitoring and management via SysMon board in dedicated slot on power backplane or in combination with rear board on standard backplane

## OpenCPSI-S.2 : Universal FPGA Board Pinout
- Provides parallel and serial connections from FPGA to Rear-IO. Signals on P6 are also seen as Rear-IO. Since it is not feasible to protect the highspeed CML signals against voltages which may arise on BaseT-Ethernet lines the system integrator shall take care not to mix these signal types on a Full-Mesh-Backplane.
- The Pinout is partial compatible with the system slot for creating FPGA centric systems. Signals deviating from the system slot specification shall be tristated when the board is inserted in the system slot.

[1] https://www.picmg.org/openstandards/compactpci-serial
